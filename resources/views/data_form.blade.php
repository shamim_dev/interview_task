<!DOCTYPE html>
<html>
<head>
<title>interview task (tmss)</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
 <style type="text/css">
      .container {
        margin-top: 40px; 
      }
  </style>
</head>
<body>




<div class="container">
  <div class="row">
    <div class="col-md-6">
      <h2>Add Employee</h2>
    </div>
    <div class="col-md-6">
        <a href="{{url('/')}}">
          <button type="button" class="btn btn-warning pull-right"> Back</button>
        </a>  
    </div>
  </div><hr>


<div class="row">
	    <div class="col-md-12">
            <div class="contacts">
               
<form action="{{url('save-data')}}" method="post">
	 @csrf
	<div class="form-group multiple-form-group input-group">       
                <div class="row">
                	<div class="col-md-3">
                        <div class="md-form">
                            <label for="fname" class="">Employee First Name:</label>
                            <input type="text" id="fname" name="fname[]" class="form-control" placeholder="Type Employee First Name" required="">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="md-form">
                            <label for="lname" class="">Employee Last Name:</label>
                            <input type="text" id="lname" name="lname[]" class="form-control" placeholder="Type Employee Last Name" required="">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="md-form">
                            <label for="email" class="">Employee Email ID:</label>
                            <input type="email" id="email" name="email[]" class="form-control" placeholder="Type Employee Email ID">
                        </div>
                    </div>

					<div class="col-md-2">
					<span class="input-group-btn">
					<button type="button" class="btn btn-success btn-add" style="margin-top: 25px;">+</button>
					</span>
					</div>
				</div>
			</div>

     <div class="row">
        <div class="col-md-5">
            <div class="md-form">
               <button class="btn btn-success pull-right" type="submit">Data Save</button>
            </div>
        </div>
    </div>

</form>
		</div>
            </div>
        </div>
	</div>


</div>



<script type="text/javascript">
	(function ($) {
    $(function () {

        var addFormGroup = function (event) {
            event.preventDefault();

            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();

            $(this)
                .toggleClass('btn-success btn-add btn-danger btn-remove')
                .html('–');

            $formGroupClone.find('input').val('');
            $formGroupClone.find('.concept').text('Phone');
            $formGroupClone.insertAfter($formGroup);

            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };

        var removeFormGroup = function (event) {
            event.preventDefault();

            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');

            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }

            $formGroup.remove();
        };

        var selectFormGroup = function (event) {
            event.preventDefault();

            var $selectGroup = $(this).closest('.input-group-select');
            var param = $(this).attr("href").replace("#","");
            var concept = $(this).text();

            $selectGroup.find('.concept').text(concept);
            $selectGroup.find('.input-group-select-val').val(param);

        }

        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };

        $(document).on('click', '.btn-add', addFormGroup);
        $(document).on('click', '.btn-remove', removeFormGroup);
        $(document).on('click', '.dropdown-menu a', selectFormGroup);

    });
})(jQuery);
</script>
</body>
</html>