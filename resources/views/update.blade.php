<!DOCTYPE html>
<html>
<head>
<title>interview task (tmss)</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
 <style type="text/css">
      .container {
        margin-top: 40px; 
      }
  </style>
</head>
<body>




<div class="container">
  <div class="row">
    <div class="col-md-6">
      <h2>Add Employee</h2>
    </div>
    <div class="col-md-6">
        <a href="{{url('/')}}">
          <button type="button" class="btn btn-warning pull-right"> Back</button>
        </a>  
    </div>
  </div><hr>


<div class="row">
	    <div class="col-md-12">
            <div class="contacts">
               
<form action="{{url('data-update-store')}}" method="post">
	 @csrf
     <input type="hidden" name="id" value="{{$employeeData->id}}">
	<div class="form-group multiple-form-group input-group">       
                <div class="row">
                	<div class="col-md-3">
                        <div class="md-form">
                            <label for="fname" class="">Employee Friest Name:</label>
                            <input type="text" id="fname" name="fname" class="form-control" placeholder="Type Employee Friest Name" required="" value="{{$employeeData->fname}}">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="md-form">
                            <label for="lname" class="">Employee Last Name:</label>
                            <input type="text" id="lname" name="lname" class="form-control" placeholder="Type Employee Last Name" required="" value="{{$employeeData->lname}}">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="md-form">
                            <label for="email" class="">Employee Email ID:</label>
                            <input type="email" id="email" name="email" class="form-control" placeholder="Type Employee Email ID" value="{{$employeeData->email}}">
                        </div>
                    </div>

				
				</div>
			</div>

     <div class="row">
        <div class="col-md-5">
            <div class="md-form">
               <button class="btn btn-success pull-right" type="submit">Data Save</button>
            </div>
        </div>
    </div>

</form>
		</div>
            </div>
        </div>
	</div>


</div>



</body>
</html>