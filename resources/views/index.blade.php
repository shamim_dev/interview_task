<!DOCTYPE html>
<html>
<head>
<title>interview task (tmss)</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
 <style type="text/css">
      .container {
        margin-top: 40px; 
      }
  </style>
</head>
<body>




<div class="container">
  <div class="row">
    <div class="col-md-6">
      <h2>Employee List</h2>
    </div>
    <div class="col-md-6">
        <a href="{{url('add-form')}}">
          <button type="button" class="btn btn-primary pull-right">Add New Employee</button>
        </a>  
    </div>
  </div><hr>

@if(session('message'))
<div class="alert alert-{{session('class')}} alert-block"><!-- danger -->
<button type="button" class="close" data-dismiss="alert">×</button> 
<strong>{{session('message')}}</strong>
</div>
@endif

  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>First name</th>
        <th>Last name</th>
        <th>Email</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
       @php $i=1; @endphp
       @foreach($employeeData as $data)
      <tr>
        <td>{{$i++}}</td>
        <td>{{$data->fname}}</td>
        <td>{{$data->lname}}</td>
        <td>{{$data->email}}</td>
        <td>
          <a href="{{url('data-update/'.$data->id)}}">
          <button type="button" class="btn btn-primary">Update</button>
        </a>  
        <a href="{{url('data-dell/'.$data->id)}}">
          <button type="button" class="btn btn-danger">Delete</button>
        </a>  
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>


</body>
</html>