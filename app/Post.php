<?php 
namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use App\Contracts\PostContract as PostContract;
use Illuminate\Database\Eloquent\Model;

class Post extends Model implements PostContract {



    protected $table = "employee";
    protected $fillable = ['id', 'fname', 'lname', 'email'];


		/*public function __construct(){
		$this->storage = Redis::connection();

		}*/

	public function fetchAll()
	{

		//return $this->get();
		$result = Cache::remember('employee_cache', function()
		{
			return $this->get();
		});

		return $result;
	}



}
