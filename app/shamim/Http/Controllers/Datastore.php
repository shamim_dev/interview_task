<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redis;
//use Illuminate\Support\Facades\Cache;


 
class Datastore extends Controller{

public function __construct () {
   $this->connection = Redis::connection();
  }


    public function index(){
    //$data["employeeData"] = $employeeData = DB::table('employee')->orderby('id', 'DESC')->get();
        //return view('index', $data);
        //Cache::put('cacheData', $data);
        //$cache = Cache::get('cacheData');
        //$redis = app()->make('redis');

//$this->connection->set("key1", $employeeData);
$data = $this->connection->get("key1");
return $data;
//return view('index', json_decode($data, true));
    

    
    }

     public function DataForm(){
        return view('data_form');
    }

     public function DataStore(Request $request){
        foreach($request->fname as $item=>$i){
        $employeeData = [
        'fname'=>$request->fname[$item],
        'lname'=>$request->lname[$item],
        'email'=>$request->email[$item],
        ];
        DB::table("employee")->insert($employeeData);
        }


        $msg = ['class'=>'success', 'message'=>"Employee Data Add Successfull"];
        return redirect()->route('home')->with($msg);
    }


    public function DataUpdat(Request $request){
        $data["employeeData"] = DB::table('employee')
        ->where('id',$request->id)
        ->first();
        return view('update', $data);
    }

     public function DataUpdatStore(Request $request){
       $inputData = [
            'fname' => $request->fname,
            'lname' => $request->lname,
            'email' => $request->email
        ];
        DB::table('employee')->where('id',$request->id)->update($inputData);
        $msg = ['class'=>'success', 'message'=>"Employee Data Update Successfull"];
        return redirect()->route('home')->with($msg);
    }

        public function DataDell(Request $request){
        DB::table('employee')->where('id',$request->id)->delete();



        $msg = ['class'=>'success', 'message'=>"Employee Data Delete Successfull"];
        return redirect()->route('home')->with($msg);
        }


}