<?php 
namespace App\Http\Controllers;
use DB;
use App\Post;
use Illuminate\Support\Facades\Redis;
use App\Contracts\PostContract as PostContract;




class BlogController extends Controller {

	public function __construct(PostContract $post){
	 	$this->post = $post;
		
	 }

	public function showBlog(){
		//DB::connection()->enableQueryLog();
		$posts = $this->post->fetchAll();
		//$posts = Post::orderBy('id', 'desc')->get();
		//$log = DB::getQueryLog();
		//print_r($log);
		return view('index')->with([ 'employeeData' => $posts]);
	}




}
