<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/



Route::get('/', 'BlogController@showBlog'); 

//Route::get('/', 'Datastore@index')->name('home');

Route::get('home', 'Datastore@index')->name('home');
Route::get('add-form', 'Datastore@DataForm')->name('add-form');
Route::post('save-data', 'Datastore@DataStore')->name('save-data');
Route::get('data-update/{id}', 'Datastore@DataUpdat')->name('data-update');
Route::post('data-update-store', 'Datastore@DataUpdatStore')->name('data-update');
Route::get('data-dell/{id}', 'Datastore@DataDell')->name('data-dell');
	
